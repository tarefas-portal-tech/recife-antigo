# Recife Antigo

recife_antigo.html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <header>
        <span>Recife Antigo</span>
        <nav>
            <li>
                <ul>Paço do Frevo</ul>
                <ul>Sinagoga Kahal Zur Israel</ul>
            </li>
        </nav>
    </header>
    <main>
        <h1>Bemvindo ao Recife Antigo!</h1>
        <h2>Sobre os Pontos Turisticos</h2>
        <p><B>Um passeio cheio de história é a Sinagoga Kahal Zur Israel, a primeira das Américas, que se instalou em Pernambuco trazendo um grande número de judeus que vieram de Amsterdã, 
        fugindo da Inquisição e em busca de paz e liberdade de culto. O local é bem preservado e tem um acervo que conta a história desse povo e leva o visitante a uma viagem no tempo, 
        ajudando a compreender a influência da população judaica na cidade.

            Endereço: R. do Bom Jesus, 197.
            Valor de entrada: R$ 10 – inteira; R$ 5 – estudantes, professores e idosos.
            Horário de funcionamento: de terça a sexta, das 9h às 16h30; domingos, das 14h às 17h30.<br>
    <section><img src="./sinagogarecife.jpg"/></section>
        
            Não dá para visitar Pernambuco e não arriscar alguns passos de frevo, afinal, o ritmo foi declarado Patrimônio Imaterial da Humanidade pela Unesco! 
        No Paço do Frevo é Carnaval o ano inteiro. Lá estão expostos fotos e documentos que contam a história do ritmo e guarda registros do carnaval pernambucano que se 
        manterão vivos para as futuras gerações. São três andares que guardam um rico acervo e, para acompanhar a visita, os corredores são embalados pelo som do frevo!

            Endereço: Praça do Arsenal da Marinha, s/nº.
            Valor de entrada: R$ 8 – inteira; R$ 4 – estudantes e maiores de 60 anos.
            Horário de funcionamento: de terça a sexta, das 9h às 17h; sábados e domingos, das 14h às 18h; fechado às segundas. Entrada para visitação até 30 minutos antes do fechamento.<br>
    <section><img src="./pacofrevo.jpeg"/></section>
        </B></p>
    </main>
    <footer>
        <ul>
            <li><a href="https://www.viajali.com.br/recife-antigo/"></a></li>
        </ul>
    </footer>  
</body>
</html>
